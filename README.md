## My personal host file based on mmotti's script (Only for Samsung Knox Firewall)

The aim of this host file has been to keep the amount of hosts as small as possible. Bigger isn't always better, especially with the effective use of wildcards and pruning of dead hosts.

https://gitlab.com/fusionjack/hosts/raw/master/hosts

This host file has been created specifically for use with Samsung Knox Firewall; It is based on the following sources:

* [Steven Black Host](https://github.com/StevenBlack/hosts)
* [lightswitch05] (https://raw.githubusercontent.com/lightswitch05/hosts/master/ads-and-tracking-extended.txt)
* [Adguard German Filter](https://filters.adtidy.org/extension/chromium/filters/6.txt)
* [Adguard Mobile Filter](https://filters.adtidy.org/extension/chromium/filters/11.txt)
* [ZeuS domain blocklist](https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist)

#### Blacklist recommendations
These domains are annoyances that cannot be included in the main host-file due to issues that may arise as a result of blocking them.
* **graph.facebook.com** (Facebook Ad Choices; can break Facebook login etc.)

#### Whitelist recommendations
These domains may need to be whitelisted for certain sites to function correctly.
* **analytics.twitter.com** (reports of broken twitter links)

#### Credits
https://github.com/mmotti/mmotti-host-file